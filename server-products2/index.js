let Promise = require('bluebird');
let sqlite = require('sqlite');
var bodyParser = require('body-parser');


const app = express();
const port = process.env.PORT || 9090;



// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

const dbPromise = Promise.resolve()
    .then(() => sqlite.open('./database.sqlite', { Promise }))
    .then(db => db.migrate({ force: 'last' }));

app.get('/product/:id', (req, res) => {
    dbPromise.then(db => {

        db.get('SELECT * FROM `product` WHERE `id`='+req.params.id).then((data) => {
            res.json(data);
        }, () => res.json({error:true}));

        return db;
    });

});

app.get('/products', (req, res) => {
    dbPromise.then(db => {

        db.all('SELECT * FROM `product`').then((data) => {
            res.json(data);
        }, () => res.json({error:true}));

        return db;
    });
});


app.get('/items', (req, res) => {
    dbPromise.then(db => {

        db.all('SELECT * FROM `cart_item` INNER JOIN `product` ON `cart_item`.`p_id`=`product`.`id`').then((data) => {
            res.json(data);
        }, () => res.json({error:true}));

        return db;
    });
});

app.post('/item/:id', (req, res) => {

    let product_id = req.params.id;
    let count = parseInt(req.body.count);


    dbPromise.then(db => {
        db.get('SELECT * FROM `product` WHERE `id`=' + product_id).then((product) => {

            if (!product) {
                res.json({error:true});
            } else {
                db.get('SELECT * FROM `cart_item` WHERE `p_id`='+product_id).then((cart_item) => {

                    if (!cart_item) {
                        db.all('INSERT INTO `cart_item`(`p_id`, `count`) VALUES('+product_id+', '+count+') ').then((data) => {
                            res.json({error:false});
                        }, () => res.json({error:true}));
                    } else {
                        db.all('UPDATE `cart_item` SET `count`='+count+' WHERE `p_id`='+product_id).then((data) => {
                            res.json({error:false});
                        }, () => res.json({error:true}));
                    }


                }, () => res.json({error:true}));
            }
        }, () => res.json({error: true}));

        return db;
    });
});

app.listen(port);