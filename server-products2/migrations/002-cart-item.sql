-- Up
CREATE TABLE IF NOT EXISTS `cart_item`(
    `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `count` INTEGER DEFAULT 0,
    `p_id` INTEGER,
    FOREIGN KEY(`p_id`) REFERENCES `product`(`id`)
);

CREATE TABLE IF NOT EXISTS `product`(
    `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `price` INTEGER DEFAULT 0,
    `name` CHAR(50),
    `desc` CHAR(100),
    `max_count` INTEGER DEFAULT 100
);


-- Down
-- DROP TABLE IF EXISTS `product`;
-- DROP TABLE IF EXISTS `cart_item`;