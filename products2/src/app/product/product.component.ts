import { Component, OnInit } from '@angular/core';
import {ProductService} from "../services/product.service";
import {ActivatedRoute} from "@angular/router";
import {CartService} from "../services/cart.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  public request;
  public count;

  constructor(public productService:ProductService, public cartService: CartService, public route: ActivatedRoute) {
    route.params.subscribe(request => this.request = request);
  }

  ngOnInit() {
  }

  public get product() { return this.productService.getProduct(this.request.id);}

  public get productItem() { return this.cartService.getItem(this.request.id);}

  public get productCount() {
    let item = this.productItem;

    return item ? item.count : 0;
  }

  public updateCartItem() {
    this.cartService.updateItem(this.request.id, this.count);
  }
}
