
import {ProductsComponent} from "./products/products.component";
import {ProductComponent} from "./product/product.component";
import {CartComponent} from "./cart/cart.component";
import {Routes} from "@angular/router";


export const routes: Routes =[
  { path: 'product/:id', component: ProductComponent },
  { path: 'products', component: ProductsComponent},
  { path: 'cart', component: CartComponent },
  { path: '', redirectTo: '/products', pathMatch: 'full' }
];
