import { Injectable } from '@angular/core';
import {ajax} from "rxjs/internal/observable/dom/ajax";

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  protected static _products = [];


  constructor() {
    ajax({ url: 'http://localhost:9090/products', responseType: 'json', crossDomain: true})
        .subscribe(
            (data) => {
              ProductService._products = data.response;
            },
            (error) => console.error(error)
        );
  }


  public getProduct(id) {
    return this.products.find((elem) => elem.id == id );
  }

  public get products() {
    return ProductService._products;
  }
}
