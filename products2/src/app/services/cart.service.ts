import { Injectable } from '@angular/core';
import {ajax} from "rxjs/internal/observable/dom/ajax";

@Injectable({
  providedIn: 'root'
})
export class CartService {
  protected static _items = [];


  constructor() {
    ajax({ url: 'http://localhost:9090/items', responseType: 'json', crossDomain: true})
        .subscribe(
            (data) => {
              CartService._items = data.response;
            },
            (error) => console.error(error)
        );
  }


  public getItem(product_id) {
    return this.items.find((elem) => elem.p_id == product_id );
  }

  public get items() {
    return CartService._items;
  }

  public updateItem(id, count) {
    this.getItem(id).count = count;
    ajax({ url: 'http://localhost:9090/item/'+id, method:'POST', body:{count: count}, responseType: 'json', crossDomain: true})
        .subscribe(
            (data) => {
              if (data.response.error == true) {
                alert('error');
              } else {
                alert('success');
              }
            },
            (error) => console.error(error)
        );
  }
}
